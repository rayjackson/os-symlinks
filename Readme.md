How To Use: 
Go to SymLinks/src/ via Terminal

Create JAR-file:
$ javac SymLinks.java //compiles SymLinks.class
$ jar cfm JAR_NAME.jar Manifest SymLinks.class //compiles into jar-file named JAR_NAME.jar

Run JAR-file
$ java -jar JAR_NAME.jar
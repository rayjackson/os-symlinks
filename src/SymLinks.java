import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SymLinks {
    public static void main(String[] args) {
        final Path origin = Paths.get("").toAbsolutePath();
        System.out.println("Origin: " + origin);
        checkInsideSandbox(origin);
    }

    private static void checkStraight(Path origin) {
        int symLinks = 0;
        for (Path curr = origin; curr != null; curr = curr.getParent()) {
            printPath(curr);
            try {
                symLinks += checkLinks(origin, curr);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        printTotal(symLinks);
    }

    // выводим только ссылки в четных папках
    private static void checkEven(Path origin) {
        int symLinks = 0;
        int count = 0;
        for (Path curr = origin; curr != null; curr = curr.getParent()) {
            if (count % 2 == 0) {
                printPath(curr);
                try {
                    symLinks += checkLinks(origin, curr);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            count++;
        }
        printTotal(symLinks);
    }

    // Поиск ссылок внутри песочницы
    private static void checkInsideSandbox(Path origin) {
        final Path sandbox = Paths.get(System.getProperty("user.home"));
        System.out.println("Sandbox: " + sandbox);
        int symLinks = 0;
        boolean isInside = origin.startsWith(sandbox); //проверка, вызывается ли программа в песочнице

        if (!isInside) {
            return; //если программа вызывается вне песочницы, то завершаем работу
        }

        for(Path curr = origin; curr != null; curr = curr.getParent()) {
            isInside = curr.startsWith(sandbox);

            if (isInside) {
                printPath(curr);
                try {
                    symLinks += checkLinks(origin, curr);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        printTotal(symLinks);
    }

    // Проверять только ссылки, которые содержат слово ${ word }
    private static void checkIfContainsWord(Path origin, String word) {
        word = word.toLowerCase();
        int symLinks = 0;
        for(Path curr = origin; curr != null; curr = curr.getParent()) {
            printPath(curr);
            String pathString = curr.toString().toLowerCase();
            if (!pathString.contains(word)) {
                continue;
            }
            try {
                symLinks += checkLinks(origin, curr);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        printTotal(symLinks);
    }

    private static int checkLinks(Path origin, Path curr) throws IOException {
        int result = 0;

        File[] listFiles = curr.toFile().listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                Path filePath = Paths.get(file.toPath().toString());
                if (Files.isSymbolicLink(filePath) &&
                        Paths.get(Files.readSymbolicLink(filePath).toString()).equals(origin)) {
                    System.out.println("Found: " + file);
                    result++;
                }
            }
        }
        return result;
    }

    private static void printTotal(int links) {
        System.out.println("Links found: " + links);
    }

    private static void printPath(Path path) {
        System.out.println("Path: " + path);
    }
}